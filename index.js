const express = require('express');
require('dotenv').config();
const dateTools = require('./utils/getDate');
const axios = require('axios');
const { WebClient } = require('@slack/web-api');

const app = express();
app.use(express.json());

const projectID = process.env.PROJECT_ID;
const slackBotToken = process.env.SLACK_BOT_TOKEN;
const slackWebhookUrl = process.env.SLACK_WEBHOOK;
const gitlabAuth = process.env.GITLAB_AUTH
const slackClient = new WebClient(slackBotToken);

const processedMRs = new Set();
const newMRs = new Set();

app.post('/gitlab-webhook', (req, res) => {
    const event = req.headers['x-gitlab-event'];
    if (event === 'Merge Request Hook') {
        const mergeRequest = req.body.object_attributes;
        if (!processedMRs.has(mergeRequest.id)) {
            const createdDate = new Date(mergeRequest.created_at);
            const currentDate = new Date();
            const daysAgo = Math.floor((currentDate - createdDate) / (1000 * 60 * 60 * 24));
            const isOlderThan7Days = daysAgo > 7;

            if (isOlderThan7Days) {
                sendSlackMessage(mergeRequest, true)
                    .then(() => {
                        console.log('Slack message for MR older than 7 days sent successfully!');
                        processedMRs.add(mergeRequest.id);
                    })
                    .catch(error => {
                        console.error('Error sending Slack message:', error.message);
                    });
            } else {
                newMRs.add(mergeRequest.id);
            }
        }
    }
    res.sendStatus(200);
});

function getGitLabApiUrl() {
    return 'https://gitlab.com';
}

async function sendSlackMessage(mergeRequest, isOlderThan7Days = false, isOlderThan3Days = false) {
    try {
        const { data } = await axios.get(`https://gitlab.com/api/v4/projects/${projectID}/merge_requests/${mergeRequest.iid}/commits`, {
            headers: {
                Authorization: `Bearer ${gitlabAuth}`,
            }
        });
        const userEmail = data[0].author_email;

        let userMention;
        const userInfoResponse = await slackClient.users.lookupByEmail({ email: userEmail });
        const userInfo = userInfoResponse.user;
        if (userInfo.id) {
            userMention = `<@${userInfo.id}>`;
        }

        if (!userMention) {
            userMention = mergeRequest.author.name;
        }
        let messageText = `${userMention} :ghost: Has opened the Merge Request: <${mergeRequest.web_url}|${mergeRequest.title}>`;

        if (isOlderThan7Days) {
            messageText = `${userMention} :ghost: This Merge Request has been open for more than 7 days.`;
        }

        if (isOlderThan3Days) {
            messageText = `${userMention} :ghost: This Merge Request hasn't been updated for more than 3 days.`;
        }

        const response = await axios.post(slackWebhookUrl, {
            blocks: [
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: messageText
                    }
                },
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: `*State of MR:* ${mergeRequest.state}`
                    }
                },
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: `*Created at:* ${dateTools.getDate(mergeRequest.created_at)}`
                    }
                },
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: `*Reviewers:* ${mergeRequest.reviewers.map(person => person.name).join(', ')}`
                    }
                },
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: `*Assignees:* ${mergeRequest.assignees.map(person => person.name).join(', ')}`
                    }
                },
                {
                    type: 'section',
                    text: {
                        type: 'mrkdwn',
                        text: '*Here is the link to the MR*'
                    },
                    accessory: {
                        type: 'button',
                        text: {
                            type: 'plain_text',
                            text: 'View the MR',
                            emoji: true
                        },
                        value: 'click_me_123',
                        url: `${mergeRequest.web_url}`,
                        action_id: 'button-action'
                    }
                },
                {
                    type: 'divider'
                },
            ]
        });

        return response.data;
    } catch (error) {
        console.error('Error sending Slack message:', error.message);
        throw error;
    }
}

async function sendNewMRSlackMessages() {
    const slackMessages = [];
    for (const mergeRequestId of newMRs) {
        const mergeRequest = await fetchMergeRequestById(mergeRequestId);
        if (mergeRequest) {
            const lastUpdatedDate = new Date(mergeRequest.updated_at);
            const currentDate = new Date();
            const daysAgo = Math.floor((currentDate - lastUpdatedDate) / (1000 * 60 * 60 * 24));
            const isOlderThan3Days = daysAgo > 3;
            slackMessages.push(sendSlackMessage(mergeRequest, true, isOlderThan3Days));
            processedMRs.add(mergeRequestId);
        }
    }
    await Promise.all(slackMessages);
    newMRs.clear();
}

async function fetchMergeRequestById(mergeRequestId) {
    try {
        const gitLabApiUrl = getGitLabApiUrl();
        const { data } = await axios.get(`${gitLabApiUrl}/api/v4/projects/${projectID}/merge_requests/${mergeRequestId}`, {
            headers: {
                Authorization: `Bearer ${gitlabAuth}`,
            }
        });
        return data;
    } catch (error) {
        console.error('Error fetching merge request:', error.message);
        return null;
    }
}

async function fetchAndSendOldMergeRequests() {
    try {
        const gitLabApiUrl = getGitLabApiUrl();
        const { data } = await axios.get(`${gitLabApiUrl}/api/v4/projects/${projectID}/merge_requests?scope=all&state=opened&per_page=999`, {
            headers: {
                Authorization: `Bearer ${gitlabAuth}`,
            }
        });

        const sevenDaysAgo = new Date();
        sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);
        const oldMRs = data.filter(mergeRequest => new Date(mergeRequest.created_at) < sevenDaysAgo);

        const slackMessages = [];
        for (const mergeRequest of oldMRs) {
            if (!processedMRs.has(mergeRequest.id)) {
                slackMessages.push(sendSlackMessage(mergeRequest, true));
                processedMRs.add(mergeRequest.id);
            }
        }

        await Promise.all(slackMessages);
    } catch (error) {
        console.error('Error occurred while fetching or sending merge requests:', error.message);
    }
}
const refetchTime = '15:00'; // 1 PM UK
const currentTime = new Date();
const [hours, minutes] = refetchTime.split(':');
const refetchDate = new Date();

refetchDate.setHours(Number(hours), Number(minutes), 0, 0);
let delay = refetchDate - currentTime;
if (delay < 0) {
    const nextDay = new Date(refetchDate);
    nextDay.setDate(refetchDate.getDate() + 1);
    delay = nextDay - currentTime;
}
setInterval(() => {
    fetchAndSendOldMergeRequests();
}, delay);

const newMRsInterval = 3 * 60 * 60 * 1000; // 3 hours
setInterval(sendNewMRSlackMessages, newMRsInterval);

app.listen(3000, () => {
    console.log('Example app listening on port 3000!');
});
