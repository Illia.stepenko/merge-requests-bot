# merge-requests-bot



## How to generate secrets

> `SLACK_BOT_TOKEN` => follow the [link](https://api.slack.com/apps/A05GF49GWCS/oauth?) => copy the token from the field **Bot User OAuth Token**  | You must be a **Collaborator** to access the link

> `PROJECT_ID` => follow the [link](https://gitlab.com/bb2-bank/apps/mobile) => look at the **Project ID:** under the project name => copy the numeric value

>`SLACK_WEBHOOK` => follow the [link](https://api.slack.com/apps/A05GF49GWCS/incoming-webhooks?) => scroll to **Webhook URLs for Your Workspace** => copy the link that starts from `https://hooks.slack.com`

>`GITLAB_AUTH` => go to the project settings => **Access Token** => look at the **Add a project access token** => enter token name => select **api** scope => create token => copy the code from **Your new project access token**
