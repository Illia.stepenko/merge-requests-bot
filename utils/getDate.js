module.exports =  {
    getDate: (date) => {
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        const newDate = new Date(date)
        return newDate.toLocaleDateString("en-US", options)
    }

}
